import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ColDef } from 'ag-grid-community'
import { CoreService } from '../../core/core.service';

@Component({
  selector: 'app-data-service-form',
  templateUrl: './data-service-form.component.html',
  styleUrl: './data-service-form.component.css'
})
export class DataServiceFormComponent {

  constructor(private formBuilder:FormBuilder,private coreService:CoreService){}

  datasource = this.formBuilder.group({
    m4_configuration: false,
    application_details: false,
    hospital: false,
  });

  entity=this.formBuilder.group({
    data_source_info:false,
    application_info:false,
    beds:false
  })

  public gridColDef: ColDef[] = [
    {
      headerName: "Column name",
      field: "column_name"
    },
    {
      headerName: "Data type",
      field: "data_type"
    },
    {
      headerName: "Actions",
      field: "actions"
    }

  ]
  public gridRowDef = [
    { column_name: "name", data_type: "VARCHAR", actions: "ADD"},
    { column_name: "aadhar_number", data_type: "VARCHAR", actions: "ADD"}

  ]
  public gridConfigDef: ColDef = {
    flex: 1,
    minWidth: 150,
    filter: 'ageTextColumnFilter',
    menuTabs: ['filterMenuTab']
  }

  /**
   * grid info for JoinBuilder stepper section
   */

  public gridColDef2:ColDef[]=[
    {
      headerName: "Left Entity",
      field: "le"
    },
    {
      headerName: "Left Expression",
      field: "lex"
    },
    {
      headerName: "Join Type",
      field: "jt"
    },
    {
      headerName: "Operator",
      field: "opt"
    },
    {
      headerName: "Right Entity",
      field: "re"
    },
    {
      headerName: "Right Expression",
      field: "rex"
    }
  ]

  public gridRowDef2=[
    { le: "Table1", lex: "alt_key", jt: "inner", opt: "=", re: "Table2", rex: "alt_key" },
    { le: "Table1", lex: "alt_key", jt: "outer", opt: "=", re: "Table2", rex: "alt_key" }
  ]

  public gridConfigDef2:ColDef={
    flex:1,
    minWidth:150,
    filter:'ageTextColumnFilter',
    menuTabs:['filterMenuTab']
  }
}
