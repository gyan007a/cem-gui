import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DmService {

  public getUrl="http://localhost:8084/getAllDataSources";
  public createUrl="http://localhost:8084/createDataSource";


  constructor(private httpClient:HttpClient) { }

  public getDataSource(){
    return this.httpClient.get(this.getUrl);
  }
  
  public createDataSource(saveData:any){
     return this.httpClient.post(this.createUrl,saveData).subscribe(
      refe=>{
        console.log(refe);
        alert("Data saved successfully");
      },
      (error)=>{
        console.error('Error'+error);
      }
     )
  }
}
