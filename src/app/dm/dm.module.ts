import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataServiceFormComponent } from './data-service-form/data-service-form.component';
import { DataServiceListComponent } from './data-service-list/data-service-list.component';
import { DataSourceFormComponent } from './data-source-form/data-source-form.component';
import { DataSourceListComponent } from './data-source-list/data-source-list.component';
import { RouterModule } from '@angular/router';
import { CoreModule } from "../core/core.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { HttpClientModule } from '@angular/common/http';
import { AgGridAngular, AgGridModule } from 'ag-grid-angular';
import { ButtonComponent } from './button/button.component';

@NgModule({
    declarations: [
        DataServiceFormComponent,
        DataServiceListComponent,
        DataSourceFormComponent,
        DataSourceListComponent,
        ButtonComponent
    ],
    imports: [
        RouterModule.forChild([
            { path: 'dataSource', component: DataSourceListComponent },
            { path: 'dataList', component: DataServiceListComponent }
        ]),
        CommonModule,
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatButtonModule,
        MatStepperModule,
        MatDialogModule,
        MatTabsModule,
        MatCheckboxModule,
        HttpClientModule,
        AgGridModule,
        AgGridAngular
    ],

    exports:[
        DataSourceListComponent,
        DataServiceListComponent
    ]
})
export class DmModule { }
