import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DmService } from '../dm.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-data-source-form',
  templateUrl: './data-source-form.component.html',
  styleUrl: './data-source-form.component.css'
})
export class DataSourceFormComponent implements OnInit {
actionType: any;

  constructor(
    private formBuilder: FormBuilder,
    private dmService: DmService,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) { }

  public dataSourceForm: any
  public opType: any

  ngOnInit() {
    if (this.dialogData != undefined) {
      this.opType = this.dialogData.opType
      console.log("opType: " + this.opType);

      if (this.opType == "create") {
        this.dataSourceForm = this.formBuilder.group({
          dbType: [''],
          host: [''],
          dbName: [''],
          //dbPort: [''],
          userName: [''],
          password: ['']
        })
      } else {
        this.dataSourceForm = this.formBuilder.group({
          dbType: [this.dialogData.formData.dbType],
          host: [this.dialogData.formData.host],
          dbName: [this.dialogData.formData.dbName],
          userName: [this.dialogData.formData.userName],
          password: [this.dialogData.formData.password]
        })
      }
    }
  }

  public onSubmit() {
    const a = this.dataSourceForm.value
    console.log(a);
    return this.dmService.createDataSource(a);
  }
}
