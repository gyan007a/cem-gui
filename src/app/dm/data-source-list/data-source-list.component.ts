import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColDef } from 'ag-grid-community';
import { DataSourceFormComponent } from '../data-source-form/data-source-form.component';
import { CoreService } from '../../core/core.service';
import { DmService } from '../dm.service';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'app-data-source-list',
  templateUrl: './data-source-list.component.html',
  styleUrl: './data-source-list.component.css'
})
export class DataSourceListComponent implements OnInit{

  constructor(private dialog: MatDialog, private dmService: DmService) {}

  ngOnInit(): void {
    this.getData();
  }

  public openDialog(eventData:any) {
    if(eventData!=undefined){
      let opType=eventData.event.target.getAttribute("action-type")
      if(opType!=undefined && (opType=="view"||opType=="edit")){
        this.invokeDialog(opType,eventData.data)
      }
    }
  }

  public invokeDialog(opType:any,data?:any){
    const dialogRef=this.dialog.open(DataSourceFormComponent,{
      data:{
        "formData":data,"opType":opType
      },
      // width:'800px',
      // height:'400px'
    })
  }

  public gridColDef: ColDef[] = [

    {
      headerName: "ID",
      field: "altKey"
    },
    {
      headerName: "DB TYPE",
      field: "dbType"
    },
    {
      headerName: "HOST NAME",
      field: "host"
    },
    {
      headerName: "DB NAME",
      field: "dbName"
    },
    {
      headerName: "DB USERNAME",
      field: "userName"
    },
    {
      headerName: "DB PASSWORD",
      field: "password"
    },
    {
      headerName: "Created Date",
      field: "createdDate"
    },
    {
      headerName: "Modified Date",
      field: "modifiedDate"
    },
    {
      headerName:"Action",
      cellRenderer:ButtonComponent
    }
  ]
  public gridRowDef = []

  public gridConfigDef: ColDef = {
    flex: 1,
    minWidth: 150,
    filter: 'ageTextColumnFilter',
    menuTabs: ['filterMenuTab']
  }

  public getData() {
    this.dmService.getDataSource().subscribe(
      (resp: any) => {
        this.gridRowDef = resp.data;
        console.log(resp)
      }
    )
  }


}
