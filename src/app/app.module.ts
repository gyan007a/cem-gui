import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { CoreModule } from './core/core.module';
import { RouterModule, Routes } from '@angular/router';
import { AgGridAngular, AgGridModule } from 'ag-grid-angular';

const routes:Routes=[
  {
    path:'dm',
    loadChildren:()=> import('./dm/dm.module').then(m=>m.DmModule)
  }
]

@NgModule({
  declarations: [
   AppComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    AgGridModule,
    AgGridAngular
  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }