import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar/top-bar.component';
import { MenuComponent } from './menu/menu.component';
import { GridComponent } from './grid/grid.component';
import { AgGridAngular, AgGridModule } from 'ag-grid-angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatMenuModule } from '@angular/material/menu';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    TopBarComponent,
    MenuComponent,
    GridComponent
  ],
  imports: [
    
    CommonModule,
    AgGridModule,
    FontAwesomeModule,
    MatMenuModule,
    HttpClientModule,
    AgGridAngular
  ],
  exports: [
    TopBarComponent,
    MenuComponent,
    GridComponent
  ]

})
export class CoreModule { }