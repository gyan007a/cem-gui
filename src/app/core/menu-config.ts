export class Config {
 static menuConfigObj={
    "DataModule":{
            "icon":"fa-solid fa-right-from-bracket",
            "subModules":[
                     {
                       "url":"dataService",
                       "resourceName":"dataServiceLink"
                     },
                     {
                       "url":"dataSource",
                       "resourceName":"dataSourceLink"
                     }
            ]
         }
  }
}