import { Component } from '@angular/core';
import { faRightToBracket,faRightFromBracket } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrl: './top-bar.component.css'
})
export class TopBarComponent {

  title = 'Customer Experience Management';
  logoImage = "https://cdn.pixabay.com/photo/2017/03/19/20/19/ball-2157465_640.png";
  logoutIcon = faRightToBracket;

}