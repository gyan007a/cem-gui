import { Injectable } from '@angular/core';
import { Config } from './menu-config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  
  getMenuConfig(): any {
    return Config.menuConfigObj;
  }

  public getUrl="http://localhost:8084/getAllDataSources";


  constructor(private httpClient:HttpClient) { }

  public getDataSource(){
    return this.httpClient.get(this.getUrl);
  }

  public createDataSource(saveData:any){

  }
}
