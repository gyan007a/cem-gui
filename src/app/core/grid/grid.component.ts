import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrl: './grid.component.css'
})
export class GridComponent {
  @Input() gridHead: any
  @Input() gridData: any
  @Input() gridConfig: any
  @Output() gridEvent = new EventEmitter()


  public onRowClicked(event:any){
    this.gridEvent.emit(event);
  }
}
